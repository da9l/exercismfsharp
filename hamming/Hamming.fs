﻿module Hamming

let distance (strand1: string) (strand2: string): int option = 
    let different a b = if a <> b then 1 else 0                    
    match strand1, strand2 with
    | s1,s2 when s1.Length <> s2.Length -> None
    | s1,s2 ->
        Seq.map2 different s1 s2
        |> Seq.sum
        |> Some        
