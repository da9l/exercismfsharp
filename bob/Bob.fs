﻿module Bob
open FsUnit.CustomMatchers

let response (input: string): string = 
    let hasNoLowerCaseLetters str =
        not (str |> String.exists (fun c -> System.Char.IsLower(c)))

    let hasAtLeastOneUpperCaseLetter str =
        str |> String.exists (fun c -> System.Char.IsUpper(c))

    let isShouting str =
        hasNoLowerCaseLetters str 
        && hasAtLeastOneUpperCaseLetter str 

    let isAsking str =
        str |> Seq.last = '?'

    let toOption = function
        | true -> Some()
        | false -> None

    let (|IsShouting|_|) str =
        str 
        |> isShouting
        |> toOption
        
    let (|IsAsking|_|) str =
        str 
        |> isAsking
        |> toOption

    let (|IsSilent|_|) str =
        str 
        |> System.String.IsNullOrWhiteSpace
        |> toOption

    match input.Trim() with
    | IsSilent  -> "Fine. Be that way!"
    | IsShouting -> "Whoa, chill out!"
    | IsAsking -> "Sure."
    | _ -> "Whatever."