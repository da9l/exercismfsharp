﻿module SpaceAge

type Planet =
    | Earth
    | Mercury
    | Venus
    | Mars
    | Jupiter
    | Saturn
    | Uranus
    | Neptune

let planetOrbitInEarthYears planet =
    match planet with
    | Earth -> 1m
    | Mercury -> 0.2408467m
    | Venus -> 0.61519726m
    | Mars -> 1.8808158m
    | Jupiter -> 11.862615m
    | Saturn -> 29.447498m
    | Uranus -> 84.016846m
    | Neptune -> 164.79132m

let round (p:int) (n:decimal) =
    System.Math.Round(n, p)
    
let spaceAge planet ageInseconds =
    let earthOrbitInSeconds = 31557600m
    ageInseconds/earthOrbitInSeconds/planetOrbitInEarthYears planet
    |> round 2