module LuciansLusciousLasagna

// TODO: define the 'expectedMinutesInOven' binding
let expectedMinutesInOven = 40

// TODO: define the 'remainingMinutesInOven' function
let remainingMinutesInOven spent = expectedMinutesInOven - spent

// TODO: define the 'preparationTimeInMinutes' function
let layerPrepTime = 2
let preparationTimeInMinutes layers = layers*layerPrepTime

// TODO: define the 'elapsedTimeInMinutes' function
let elapsedTimeInMinutes layers inOven = 
    layers
    |> preparationTimeInMinutes
    |> (+) inOven
