﻿module ErrorHandling
open System

let handleErrorByThrowingException() = 
    raise (Exception())

let handleErrorByReturningOption (input:string) = 
    match System.Int32.TryParse input with
    | true, v -> Some v
    | false,_ -> None
let handleErrorByReturningResult (input:string) = 
    match System.Int32.TryParse input with
    | true,v -> Ok v
    | false,_ -> Error "Could not convert input to integer"

let bind switchFunction twoTrackInput = 
    match twoTrackInput with
    | Ok v -> switchFunction v
    | Error m -> Error m 

let cleanupDisposablesWhenThrowingException (resource:IDisposable) = 
    resource.Dispose()
    raise (Exception())