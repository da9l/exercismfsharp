module OcrNumbers

let zero = 
        [ " _ "
          "| |"
          "|_|"
          "   " ]

let one = 
        [ "   "
          "  |"
          "  |"
          "   " ]

let two =
        [ " _ "
          " _|"
          "|_ "
          "   " ]

let three = 
        [ " _ "
          " _|"
          " _|"
          "   " ]

let four = 
        [ "   "
          "|_|"
          "  |"
          "   " ]  
let five = 
        [ " _ "
          "|_ "
          " _|"
          "   " ]
let six = 
        [ " _ "
          "|_ "
          "|_|"
          "   " ]
let seven = 
        [ " _ "
          "  |"
          "  |"
          "   " ]

let eight =
        [ " _ "
          "|_|"
          "|_|"
          "   " ]

let nine =
        [ " _ "
          "|_|"
          " _|"
          "   " ]                        


let numbers = 
    [ zero,     "0"
      one,      "1"
      two,      "2"
      three,    "3"
      four,     "4"
      five,     "5"
      six,      "6"
      seven,    "7"
      eight,    "8"
      nine,     "9"]
    |> Map.ofList

let hasRowLengthMod3 =
    List.forall (fun row -> String.length row % 3 = 0)

let hasMultipleOf4Lines lines =
    List.length lines % 4 = 0

let (|LinesDividableBy4|_|) lines = 
    if hasMultipleOf4Lines lines && hasRowLengthMod3 lines then
        Some()
    else 
        None    

let ocrParseSingleDigit input =
    match numbers.TryFind(input) with
    | Some n -> n
    | None -> "?"

let chunkRowStrings (strList:string list) =
    strList 
    |> List.map (Seq.chunkBySize 3 >> List.ofSeq >> List.map System.String.Concat)

let parseOcrLine (ocrList : string list)  = 
    ocrList
    |> chunkRowStrings
    |> List.transpose
    |> List.map ocrParseSingleDigit
    |> String.concat ""

let convert ocrList =
    match ocrList with
    | LinesDividableBy4 ->
        ocrList
        |> List.chunkBySize 4
        |> List.map parseOcrLine
        |> String.concat ","
        |> Some 
    | _ -> None
