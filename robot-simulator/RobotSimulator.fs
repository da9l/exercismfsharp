﻿module RobotSimulator

type Direction = North | East | South | West
type Position = int * int
type Robot = { direction: Direction; position: Position }
let create direction position = 
    {
        direction = direction
        position = position
    }
let right = function
    | North -> East
    | East -> South
    | South -> West
    | West -> North
let left = right >> right >> right
let turn turnStrategy robot  =
    { robot with direction = robot.direction |> turnStrategy }
let turnLeft robot =
    robot |> turn left
let turnRight robot = 
    robot |> turn right
let advanceInDirection (x,y) = function
    | North -> x,y+1
    | East -> x+1,y
    | South -> x,y-1
    | West -> x-1,y
let advance { direction = d ; position = p } =
    { direction=d ; position = advanceInDirection p d }
let action = function 
    | 'R' -> turnRight
    | 'L' -> turnLeft
    | 'A' -> advance 
    | _ -> id
let move r i = action i r
let instructions instructions' robot =
    instructions' |> Seq.fold move robot 