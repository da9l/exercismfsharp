﻿module GradeSchool

let empty: Map<int, string list> = Map.empty

let grade (number: int) (school: Map<int, string list>): string list =
    match Map.tryFind number school with
    | Some gradeRoster -> gradeRoster
    | None -> []
let add (student: string) (gradeNumber: int) (school: Map<int, string list>): Map<int, string list> = 
    let newGradeRoster = 
        student :: grade gradeNumber school
        |> List.sort
    Map.add gradeNumber newGradeRoster school
let roster (school: Map<int, string list>): (int * string list) list = 
    Map.toList school