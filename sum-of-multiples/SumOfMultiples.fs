﻿module SumOfMultiples

let sumOfMultiples numbers upperBound = 
    let isMultiple num =
        numbers
        |> Seq.ofList 
        |> Seq.exists (fun n -> num%n=0)
    seq {
        for i in 0 .. upperBound - 1 do
            if isMultiple i then yield i
    }
    |> Seq.sum