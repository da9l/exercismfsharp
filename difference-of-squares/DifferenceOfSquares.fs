﻿module DifferenceOfSquares
open Core.Operators

let square n = n*n

let numberSeq number =
    seq {1..number}

let squareOfSum = 
    numberSeq >> Seq.sum >> square

let sumOfSquares = 
    numberSeq >> Seq.sumBy square

let differenceOfSquares (number: int): int = 
    squareOfSum number - sumOfSquares number
    |> abs