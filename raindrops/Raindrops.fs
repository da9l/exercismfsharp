﻿module Raindrops
let factorDrops = [
    (3,"Pling")
    (5,"Plang")
    (7,"Plong") ]

let convert (number: int): string =
    let findDrops =
        factorDrops
        |> Seq.filter (fun (factor, _) -> number % factor = 0)
        |> Seq.fold (fun drops (_,drop) -> drops + drop) ""
    match findDrops with
    | "" -> string number
    | drops -> drops