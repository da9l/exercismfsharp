﻿module Accumulate
let accumulate<'a, 'b> (func: 'a -> 'b) (input: 'a list): 'b list = 
    let rec operateOnList innerInput=
        match innerInput with
        | [] -> []
        | head::tail -> func head :: operateOnList tail 
    operateOnList input