module Accumulate
let accumulate<'a, 'b> (func: 'a -> 'b) (input: 'a list): 'b list = 
    let rec operateOnList acc = function
        | [] -> acc
        | head::tail -> operateOnList (func head::acc) tail
    let rec rev acc = function
        | [] -> acc
        | head::tail -> rev (head::acc) tail
    operateOnList [] input
    |> rev []
