﻿module ArmstrongNumbers
let int2intList number =
    number |> string |> Seq.map (string >> int) |> Seq.toList
let isArmstrongNumber (number: int): bool =
    let numbers = number |> int2intList
    number = (numbers |> List.sumBy (fun n -> pown n numbers.Length))
