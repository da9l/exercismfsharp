module Clock

type Clock = {
    Hours : int
    Minutes : int
}
let create (hours : int) (minutes : int) = 
    let positiveMinutesModulo minutes =
        let minutesInDay = 24*60
        let minutesModulo = minutes % minutesInDay
        minutesModulo + if minutes >= 0 then 0 else minutesInDay
    let totalMinutes = hours*60 + minutes |> positiveMinutesModulo
    {
        Minutes = totalMinutes%60
        Hours = totalMinutes/60%24
    } 

let add minutes clock = 
    create clock.Hours (clock.Minutes + minutes)

let subtract minutes clock =
    add -minutes clock

let display clock = 
    sprintf "%02d:%02d" clock.Hours clock.Minutes