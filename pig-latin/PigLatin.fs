﻿module PigLatin
open System.Text.RegularExpressions

let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None

let translate input = 
  let split (str:string) = 
    str.Split [|' '|]
  let translateWord word = 
    let ay = "ay"
    let vowelSoundPrefix="^([aeiouåäö]|yt|xr)(.*)"
    let consonantSoundPrefix="^([csxz]qu|qu|[bcdfghjklmnpqrstvwxyz][bcdfghjklmnpqrstvwxz]*)(.*)"
    match word with
    | Regex vowelSoundPrefix [_;_] -> sprintf "%s%s" word ay
    | Regex consonantSoundPrefix [prefix;rest] -> sprintf "%s%s%s" rest prefix ay 
    | _ -> word
  input
  |> split
  |> Array.map translateWord
  |> String.concat " "