﻿module QueenAttack
let onBoard i =
    i >= 0 && i<8
let create (x,y) =
    onBoard x && onBoard y
let canAttack (x1,y1) (x2,y2) =
    let sameRow = x1=x2
    let sameColumn = y1=y2
    let sameDiagonal() = (x1-x2)/(y1-y2) |> abs = 1
    sameRow || sameColumn || sameDiagonal()