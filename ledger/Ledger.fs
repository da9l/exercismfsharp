﻿module Ledger

open System
open System.Globalization

type Entry = { dat: DateTime; des: string; chg: int }
type LedgerLocale = {
    Header : string
    DateFormat : string
    AmountFormat : string -> string -> string
    NegativeAmountFormat : string -> string -> string
    ChangeToString : float -> string
}

let changeToString (locale:CultureInfo) (change:float) =
    change.ToString("#,#0.00", locale)

let ledgerLocale (locale:string) = 
    let culture = CultureInfo locale
    match locale with
    | "nl-NL" -> 
        {
            Header = "Datum      | Omschrijving              | Verandering  " 
            DateFormat = "dd-MM-yyyy"
            AmountFormat = sprintf "%s %s "
            NegativeAmountFormat = sprintf "%s %s"
            ChangeToString = changeToString culture     
        }
    | "en-US" | _ -> 
        {
            Header = "Date       | Description               | Change       "
            DateFormat = "MM\/dd\/yyyy"
            AmountFormat = sprintf "%s%s "
            NegativeAmountFormat = sprintf "(%s%s)"
            ChangeToString = Math.Abs >> changeToString culture
        }

let mkEntry date description change = { dat = DateTime.Parse(date, CultureInfo.InvariantCulture); des = description; chg = change }
        
let formatLedger currency locale entries =
    let ledgerLocale = ledgerLocale locale

    let currencyChar = 
        match currency with
        | "USD" -> "$"
        | "EUR" | _ -> "€"

    let paddedDescription (description:string)= 
        if description.Length <= 25 then 
            description.PadRight(25)
        else 
            description.[0..21] + "..."

    let padleft13 (input:string) =
        input.PadLeft(13)

    let amountToString (change:float) : string = 
        if change < 0.0 then 
            change
            |> ledgerLocale.ChangeToString
            |> ledgerLocale.NegativeAmountFormat currencyChar
        else 
            change 
            |> ledgerLocale.ChangeToString
            |> ledgerLocale.AmountFormat currencyChar
        |> padleft13            

    let dateToString (date:DateTime) = 
        date.ToString(ledgerLocale.DateFormat)

    let floatChange change =
        float change / 100.0

    let entryToString entry =
        floatChange entry.chg 
        |> amountToString
        |> sprintf "\n%s | %s | %s" (dateToString entry.dat) (paddedDescription entry.des) 

    let entriesToString entries =
        entries
        |> List.sortBy (fun x -> (x.dat, x.des, x.chg))
        |> List.fold (fun res e -> res + (entryToString e )) ""

    entries
    |> entriesToString
    |> sprintf "%s%s" ledgerLocale.Header 
