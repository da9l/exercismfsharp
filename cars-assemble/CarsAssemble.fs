module CarsAssemble

let carsPerSpeed = 221


let errorRate = function
    | 0 -> 0.0
    | n when n >= 1 && n <= 4 -> 1.0
    | n when n >= 5 && n <= 8 -> 0.9
    | 9 -> 0.8
    | 10 -> 0.77
    | _ -> 1.0

let productionRatePerHour (speed: int): float =
    speed*carsPerSpeed
    |> float
    |> (*) (errorRate speed)
let workingItemsPerMinute(speed: int): int =
    (productionRatePerHour speed) / 60.0
    // |> System.Math.Round
    |> int