﻿module TreeBuilding

type Record = { RecordId: int; ParentId: int }
type Tree = 
    | Branch of int * Tree list
    | Leaf of int

let recordId t = 
    match t with
    | Branch (id, c) -> id
    | Leaf id -> id

let isBranch t = 
    match t with
    | Branch (id, c) -> true
    | Leaf id -> false

let children t = 
    match t with
    | Branch (id, c) -> c
    | Leaf id -> []

let validateRecord r previousId = 
    if (r.RecordId <> 0 && (r.ParentId > r.RecordId || r.ParentId = r.RecordId)) then
        failwith "Nodes with invalid parents"
    if r.RecordId <> previousId + 1 then
        failwith "Non-continuous list"
    r    

let validateRecords records =
    match records with
    | [] -> failwith "Empty input"
    | root :: _ when root.ParentId <> 0 || root.RecordId <> 0 ->
        failwith "Root node is invalid"
    | recs ->
        recs 
        |> List.mapFold (fun prevId r -> validateRecord r prevId, r.RecordId) -1
        |> fst


let createGroupedNodeArray input = 
    input
    |> List.tail 
    |> List.map (fun x -> (x.ParentId, x.RecordId)) 
    |> List.groupBy fst 
    |> List.sortBy fst 
    |> List.map (snd >> List.map snd >> List.sort)
    |> Array.ofList


let rec buildTreeFromArray key (branches:int list []) : Tree =
    let convertRecordsToSubTrees =
        List.map (fun i -> buildTreeFromArray i branches)
    match key < branches.Length with
    | true -> 
        Branch (key, convertRecordsToSubTrees branches.[key])
    | _ -> 
        Leaf key
 
let buildTree  = 
    List.sortBy (fun x -> x.RecordId)
    >> validateRecords
    >> createGroupedNodeArray
    >> buildTreeFromArray 0
