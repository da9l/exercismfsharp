﻿module BinarySearchTree
open System.Diagnostics.Tracing

type Node =
    {
        value : int
        left : Node option
        right : Node option
    }
let data node =  node.value
let left node = node.left
let right node = node.right
    
let rec insertNode tree newValue =
    match tree with
    | None -> {value=newValue; left=None; right=None}
    | Some t -> 
        if newValue > t.value 
        then {t with right=insertNode t.right newValue}
        else  {t with left=insertNode t.left newValue}
    |> Some    
       
let create items = 
    items 
    |> List.fold insertNode None
    |> Option.get

let rec sortData node =
    match node with 
    | None -> []
    | Some n -> sortData n.left @ n.value :: sortData n.right

let sortedData node =
    sortData (Some node)