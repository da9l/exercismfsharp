﻿module DotDsl

type Attribute = string*string

type Node = {
    key : string
    attrs : Attribute list
}

type Edge = {
    left : string
    right : string
    attrs : Attribute list
}

type Child = 
    | Node of Node
    | Edge of Edge
    | Attribute of Attribute

let graph children = 
    children
    |> List.sortBy (fun c ->
        match c with
        | Attribute (attr,_) -> attr
        | Edge {left = l; right = _; attrs = _} -> l
        | Node {key = k; attrs = _} -> k)

let attr key value = Attribute (key,value)

let node key attrs = Node {
    key = key
    attrs = attrs
}

let edge left right attrs = Edge {
    left = left
    right = right
    attrs = attrs
}        

let attrs graph = 
    graph
    |> List.choose (function
        | Attribute a -> Some (Attribute a)
        | _ -> None )
       
let nodes graph = 
    graph
    |> List.choose (function
        | Node n -> Some (Node n)
        | _ -> None )
let edges graph = 
    graph
    |> List.choose (function
        | Edge e -> Some (Edge e)
        | _ -> None) 
