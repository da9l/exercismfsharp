module LogLevels

open System.Text.RegularExpressions

let (|Regex|_|) pattern input =
    let m = Regex.Match(input, pattern)
    if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
    else None

type LogLevel =
    | Error of string
    | Warning of string
    | Info of string
    | Unknown of string * string
    | Incorrect of string

[<AutoOpen>]
module LogLevel =
    let fromString = function
    | Regex "\[([A-Z]+)\]:\s*(.+)\s*$" [level; message] ->
        let message' = message.Trim()
        match level with
        | "WARNING" -> Warning message'
        | "ERROR" -> Error message'
        | "INFO" -> Info message'
        | other -> Unknown (other,message')
    | incorrect -> Incorrect incorrect

    let logLevelString = function
    | Error _ -> "error"
    | Warning _ -> "warning"
    | Info _ -> "info"
    | Unknown (s,_) -> s.ToLower()
    | Incorrect i -> $"Incorrect: {i.ToLower()}"

    let logLevelMessage = function
    | Error message
    | Warning message
    | Info message
    | Unknown (_,message) -> message
    | Incorrect i -> $"Incorrect: {i}"

let message (logLine: string): string = 
    logLine |> fromString |> logLevelMessage
let logLevel(logLine: string): string = 
    logLine |> fromString |> logLevelString
let reformat(logLine: string): string = 
    let log = logLine |> fromString
    $"{log |> logLevelMessage} ({log |> logLevelString})"