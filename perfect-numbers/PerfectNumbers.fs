﻿module PerfectNumbers

let divisors number =
    seq {1..number-1}
    |> Seq.choose (fun i -> 
        if number % i = 0 
        then 
            Some i
        else
            None)

type Classification = Perfect | Abundant | Deficient 
let aliquot = divisors >> Seq.sum    

let classify n : Classification option = 
    match n with
    | 0 | -1 -> None
    | _ -> 
        match aliquot n with
        | x when x = n -> Some Classification.Perfect
        | x when x > n -> Some Classification.Abundant
        | x when x < n -> Some Classification.Deficient
        | _ -> None



