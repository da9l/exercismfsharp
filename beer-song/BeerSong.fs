﻿module BeerSong
let recite (startBottles: int) (takeDown: int) = 
    let firstToUpper (s: string) =
        s.[0..0].ToUpper() + s.[1..]
    let bottlesInText bottles =
        match bottles with
        | 1 -> "1 bottle"
        | 0 -> "no more bottles"
        | -1 -> "99 bottles"
        | _ -> sprintf "%i bottles" bottles
    let beerAction bottles = 
        match bottles with
        | 0 -> "Go to the store and buy some more"
        | 1 -> "Take it down and pass it around"
        | _ -> "Take one down and pass it around"
 
    let rec reciteFor acc noBottles bottleNo =
        let firstLine =
            let bottleText = bottlesInText noBottles
            sprintf "%s of beer on the wall, %s of beer." <| bottleText >> firstToUpper <| bottleText
        let secondLine = 
            sprintf "%s, %s of beer on the wall." <| beerAction noBottles <| bottlesInText (noBottles - 1)
        match bottleNo with
        | 0 -> acc
        | _ -> reciteFor <| secondLine :: firstLine :: "" :: acc <| noBottles - 1 <| bottleNo - 1

    reciteFor [] startBottles takeDown
    |> List.rev
    |> List.tail