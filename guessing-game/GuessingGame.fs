module GuessingGame

let reply (guess: int): string = 
    match guess with
    | 42 -> "Correct"
    | 41
    | 43 -> "So close"
    | x when x < 42 - 1 -> "Too low"
    | x when x > 42 + 1 -> "Too high"
    | _ -> "Unknown"
