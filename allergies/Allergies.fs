﻿module Allergies

open System

[<System.FlagsAttribute>]
type Allergen =
    | Eggs = 1
    | Peanuts = 2
    | Shellfish = 4
    | Strawberries = 8
    | Tomatoes = 16
    | Chocolate = 32
    | Pollen = 64
    | Cats = 128

let allergicTo codedAllergies allergen =
    int allergen &&& codedAllergies = int allergen

let allAllergens =
    Enum.GetValues typeof<Allergen>
    :?> Allergen[]
    |> List.ofArray

let list codedAllergies =
    allAllergens
    |> List.filter (allergicTo codedAllergies)