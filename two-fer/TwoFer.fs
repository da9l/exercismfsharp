﻿module TwoFer

let name input = 
    match input with
    | Some name -> name
    | None -> "you"
    |> sprintf "One for %s, one for me."