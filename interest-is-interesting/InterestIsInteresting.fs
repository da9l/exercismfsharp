module InterestIsInteresting

let interestRate (balance: decimal): single =
   match balance with
   | n when n < 0.0m -> -3.213f
   | n when n < 1000.0m -> 0.5f
   | n when n >= 1000.0m && n < 5000.0m -> 1.621f
   | _ -> 2.475f

let interestFactor balance = 
   (interestRate >> decimal) balance / 100.0m + 1.0m

let annualBalanceUpdate(balance: decimal): decimal =
   interestFactor balance * balance

let amountToDonate(balance: decimal) (taxFreePercentage: float): int =
   if balance < 0.0m then 0
   else   
      balance * (decimal taxFreePercentage) / 100.0m * 2.0m
      |> int