﻿module CollatzConjecture

let steps (number: int): int option = 
    let addOne sum =
        Option.map (fun i -> i+1) sum
    let rec countSteps sum number =
        match number with
        | 1 -> sum
        | x when x<1 -> None
        | x when x%2=0 -> x/2 |> countSteps (addOne sum)
        | x -> 3*x+1 |> countSteps (addOne sum)
    countSteps (Some 0) number