﻿module KindergartenGarden

// TODO: define the Plant type
type Plant =  
    Radishes = 'R' 
    | Clover = 'C'
    | Grass = 'G'
    | Violets = 'V'
    | Empty = '.'

let students = [
    "Alice"
    "Bob"
    "Charlie"
    "David"
    "Eve"
    "Fred"
    "Ginny"
    "Harriet"
    "Ileana"
    "Joseph"
    "Kincaid"
    "Larry"
]
let plantMap c =
    match c with
    | 'R' -> Some Plant.Radishes
    | 'C' -> Some Plant.Clover
    | 'G' -> Some Plant.Grass
    | 'V' -> Some Plant.Violets
    | '.' -> Some Plant.Empty
    | _ -> Some Plant.Empty

let split (twoLines:string) =
    let lines = twoLines.Split [|'\n'|]
    (lines.[0],lines.[1])

let pair l =
    let rec loop acc = function
        | [] | [_] -> List.rev acc
        | h1 :: h2 :: tl -> loop ((h1, h2) :: acc) tl
    loop [] l

let toPlantPairs row =
    row |> Seq.map (plantMap >> Option.get) |> Seq.toList |> pair

let plants diagram student = 
    let (firstRow,secondRow) = diagram |> split
    let fr = firstRow |> toPlantPairs
    let sr = secondRow |> toPlantPairs
    let studentPlants = List.map2 (fun (f1,f2) (s1,s2) -> [f1;f2;s1;s2]) fr sr
    let studentPlantsMap = 
        studentPlants 
        |> List.zip (students |> List.take studentPlants.Length) 
        |> Map.ofList
    studentPlantsMap.[student]
